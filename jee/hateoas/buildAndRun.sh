#!/bin/sh
mvn clean package && docker build -t de.bender/hateoas .
# docker rm -f hateoas || true && docker run -d -p 8080:8080 -p 4848:4848 -v $(pwd)/dist:/opt/glassfish5/glassfish/domains/domain1/autodeploy --name hateoas de.bender/hateoas
docker rm -f hateoas || true && docker run -d -p 8080:8080 -p 4848:4848 --name hateoas de.bender/hateoas
