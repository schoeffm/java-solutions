// Copyright (c) 2011-2018 BMW Group. All rights reserved.

package de.bender.customer.boundary;

import de.bender.customer.entity.Customer;
import de.bender.ping.boundary.PingResource;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

@Path("customer")
public class CustomerFacade {

    @Inject
    CustomerResource customerResource;

    @Context
    UriInfo uriInfo;

    @GET
    public Response findAllCustomer() {
        return Response.ok(customerResource.findAll()).build();
    }

    @GET
    @Path("{id}")
    public Response findCustomerById(@PathParam("id") final String id) throws URISyntaxException {
        Optional<Customer> customerOps = customerResource.findById(id);

        if (! customerOps.isPresent()) {
            return Response.status(Response.Status.NOT_FOUND)
                    .header("X-Msg", "Probier mal die id == 1")
                    .build();
        }

        Response.ResponseBuilder ok = Response.ok(customerOps.get());

        System.out.println(uriInfo.getPath());
        System.out.println(uriInfo.getBaseUri());
        System.out.println(uriInfo.getAbsolutePath());
        System.out.println(uriInfo.getRequestUri());

        // that's correct
        URI pingId = uriInfo.getBaseUriBuilder()
                .path("ping")
                .path(PingResource.class, "pingById")
                .build("4711");

        // that's the wrong order to path-parts
        URI pingWrong = uriInfo.getBaseUriBuilder()
                .path(PingResource.class, "pingById")
                .path("ping")
                .build("4711");

        URI ping = uriInfo.getBaseUriBuilder().path(PingResource.class).build();

        Link link = Link.fromUri(ping)
                .title("Ping-Resource")
                .rel("ping")
                .type("application/json")
                .build();

        Link linkCompletelyWrong = Link.fromResource(PingResource.class)
                .title("Link.fromResource-does-not-work")
                .rel("linkCompletelyWrong")
                .build();

        Link foo = Link.fromUri(pingId).title("Ping-Resource").rel("ping").type("application/json")
                .build();
        Link foo2 = Link.fromUri(pingId).title("Most-Expressive-Approach").rel("powerPing").type("application/json")
                .param("method", "DELETE")
                .build();

        Link relativizedLink = Link.fromUri("foo/bar/buzz")
                .rel("dome").type("text/plain").title("Just To Demo relativizedLinks")
                .baseUri("http://localhost/")
                .buildRelativized(new URI("http://localhost/foo"));

        ok.link(pingId, "pingById");
        ok.link(pingWrong, "pingWrong_doesNotExist");
        ok.links(link, linkCompletelyWrong, foo, foo2, relativizedLink);

        for(String uri : uriInfo.getMatchedURIs()) {
            System.out.println("getMatchedURIs: " + uri);
        }

        for(Object match : uriInfo.getMatchedResources()) {
            System.out.println("getMatchedResources: " + match.getClass().getName());
        }

        return ok.build();
    }
}