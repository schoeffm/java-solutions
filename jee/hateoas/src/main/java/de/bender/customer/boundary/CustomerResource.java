// Copyright (c) 2011-2018 BMW Group. All rights reserved.

package de.bender.customer.boundary;

import de.bender.customer.entity.Customer;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class CustomerResource {

    static final List<Customer> customerStorage = Arrays.asList(
            new Customer("1", "Arnold", "Schwarzenegger"),
            new Customer("2", "Josef", "Hinteregger"),
            new Customer("3", "Sepp", "Herbert"),
            new Customer("4", "Luke", "Skywalker"),
            new Customer("5", "Bilbo", "Beutlin")
    );

    public List<Customer> findAll() {
        return customerStorage;
    }

    public Optional<Customer> findById(String id) {
        return customerStorage.stream()
                .filter(c -> c.getId().equalsIgnoreCase(id))
                .findFirst();
    }
}