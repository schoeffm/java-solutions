package de.bender.ping.boundary;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

/**
 *
 * @author airhacks.com
 */
@Path("ping")
public class PingResource {

    @GET
    public Response ping() {
        return Response.ok("Enjoy Java EE 8!").build();
    }

    @GET
    @Path("{id}")
    public Response pingById(@PathParam("id") String id) {
        return Response.ok("Enjoy Java EE 8! " + id).build();
    }
}
