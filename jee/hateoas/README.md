# Build
mvn clean package && docker build -t de.bender/hateoas .

# RUN

docker rm -f hateoas || true && docker run -d -p 8080:8080 -p 4848:4848 --name hateoas de.bender/hateoas 