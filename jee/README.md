# Java Solutions

## Get started quickly

When dealing with JEE project setups I tend to use the [JEE 8 archetype from Adap Bien][jee8archetype] to set thing up quickly.

Use the followin' command and scaffold a project:

    mvn archetype:generate -DarchetypeGroupId=com.airhacks -DarchetypeArtifactId=javaee8-essentials-archetype -DarchetypeVersion=0.0.2


[jee8archetype]:http://www.adam-bien.com/roller/abien/entry/java_ee_8_essentials_archetype
