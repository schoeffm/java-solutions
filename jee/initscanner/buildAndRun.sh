#!/bin/sh
mvn clean package && docker build -t de.bender/initscanner .
docker rm -f initscanner  || true && docker run -d -p 8080:8080 -p 4848:4848 --name initscanner de.bender/initscanner
