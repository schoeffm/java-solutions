package de.bender;

import de.bender.scanner.StartupInitializer;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

/**
 * The EJB approach of course only works when executed in a full-fledged application server. For that to work
 * you'll have to create a {@link Singleton}-EJB and annotate it additionally with {@link Startup}. Since it's
 * a singleton without being called from outside the {@link PostConstruct}-lifecycle hook is most suitable to
 * do all necessary initialization (since you can access injected members etc.).<p/>
 *
 * Again we use the marker-interface approach to pick up all relevant instances (see also
 * {@link ContextListenerApproachForStartupListening} as well as {@link StartupInitializer}).<p/>
 *
 * <b>Notice:</b> Don't know if this is an implementation detail or not - but the EJB approach was/gets
 * triggered before the {@link ContextListenerApproachForStartupListening}
 *
 * @see Startup
 * @see Singleton
 * @see StartupInitializer
 */
@Startup
@Singleton
public class EjbApproachForStartupLiestening {

    @Inject
    Instance<StartupInitializer> allInitializers;

    @PostConstruct
    void init() {
        System.out.println("EJB approach triggers processing ... ");

        allInitializers.stream()
                .sorted()
                .forEach(StartupInitializer::onStartup);

        System.out.println("EJB approach done processing!");
    }
}
