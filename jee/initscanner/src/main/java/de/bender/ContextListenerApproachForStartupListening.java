package de.bender;


import de.bender.scanner.StartupInitializer;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * The {@link ServletContextListener}-approach works when you have a web-application (and thus a servlet-context)
 * which is most probably the case if you do any kind of microservice.<br/>
 * For this to work you'll have to implement the {@link ServletContextListener}-interface, chose what default-method
 * to overwrite and annotate that implementation with {@link WebListener} (see also javadoc of
 * {@link ServletContextListener}).<p/>
 * The processing of <i>all-kinds-of-X</i> is realized here using a marker-interface - so all instances of
 * {@link de.bender.scanner.StartupInitializer} are injected and subsequently processed (you can add <i>metadata</i> to that interface
 * - here we used a {@link de.bender.scanner.StartupInitializer.Precedence} to fine-tune further processing).
 *
 * @see ServletContextListener
 * @see de.bender.scanner.StartupInitializer
 */
@WebListener
public class ContextListenerApproachForStartupListening implements ServletContextListener {

    @Inject
    Instance<StartupInitializer> allInitializers;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("ServletContextListener triggers processing ... ");

        // bring given initializers in order and call their onStartup-method -> done!
        allInitializers.stream()
                .sorted()
                .forEach(StartupInitializer::onStartup);

        System.out.println("ServletContextListener done processing!");
    }
}
