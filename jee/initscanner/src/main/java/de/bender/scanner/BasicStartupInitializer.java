package de.bender.scanner;


import static java.lang.String.format;

public class BasicStartupInitializer implements StartupInitializer {


    @Override
    public void onStartup() {
        System.out.println(format("%s was called: Precedence of %s", this, this.getPreceedence()));
    }

    @Override
    public String toString() {
        return "BasicStartupInitializer{}";
    }
}
