package de.bender.scanner;

import static java.lang.String.format;

public class ImportantStartupInitializer implements StartupInitializer {

    @Override
    public void onStartup() {
        System.out.println(format("%s was called: Precedence of %s", this, this.getPreceedence()));
    }

    @Override
    public Precedence getPreceedence() {
        return Precedence.of(500);
    }

    @Override
    public String toString() {
        return "ImportantStartupInitializer{}";
    }

}
