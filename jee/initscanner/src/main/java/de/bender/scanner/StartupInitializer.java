package de.bender.scanner;

import java.util.Comparator;

public interface StartupInitializer extends Comparable<StartupInitializer> {

    void onStartup();

    default Precedence getPreceedence() {
        return Precedence.of(1000);
    }

    @Override
    default int compareTo(StartupInitializer o) {
        return this.getPreceedence().weigth - o.getPreceedence().weigth;
    }

    final class Precedence {
        private final int weigth;
        private Precedence(int weight) {
            this.weigth = weight;
        }

        public static Precedence of(int weight) {
            return new Precedence(weight);
        }

        @Override
        public String toString() {
            return "Precedence{" +
                    "weigth=" + weigth +
                    '}';
        }
    }
}
