package de.bender.scanner;

import static org.junit.Assert.assertThat;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class StartupInitializerTest {


    @Test
    public void initializer_isOrderedAccordingToPrecedence() {
        // given
        Collection<StartupInitializer> initializers = new ArrayList<>();
        initializers.add(new BasicStartupInitializer());
        initializers.add(new ImportantStartupInitializer());

        // when
        List<StartupInitializer> result = initializers.stream().sorted().collect(Collectors.toList());

        assertThat(result.get(0), CoreMatchers.instanceOf(ImportantStartupInitializer.class));
        assertThat(result.get(1), CoreMatchers.instanceOf(BasicStartupInitializer.class));
    }
}