# Initscanner

Demonstrates two things:

- how to setup a Startup-Listener (a Class which gets called right when your application gets initialized)
- how to process _all beans of a specific type_
