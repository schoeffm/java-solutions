package de.bender.annotation;

import javax.annotation.PostConstruct;

@Processor
public class BasicProcessor implements IProcessor{

    @PostConstruct
    void init() {
        System.out.println("BasicProcessor was created and is ready");
    }
}
