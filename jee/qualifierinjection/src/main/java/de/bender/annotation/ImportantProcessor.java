package de.bender.annotation;

import javax.annotation.PostConstruct;

@Processor(precedence = 500)
public class ImportantProcessor implements IProcessor {

    @PostConstruct
    void init() {
        System.out.println("ImportantProcessor was created and is ready");
    }
}
