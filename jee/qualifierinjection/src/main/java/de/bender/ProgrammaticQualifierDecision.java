package de.bender;

import de.bender.annotation.IProcessor;
import de.bender.annotation.Processor;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.enterprise.util.AnnotationLiteral;
import javax.inject.Inject;

/**
 * Looking at {@link UserOfQualifiedBeans} you may don't like that you'll have to annotate the
 * injection point and thus, if you'd like to change the injected instance, you have to change the
 * source code whenever you'd like to switch behavior.<p/>
 * There's a fix for that - a programmatic way of deciding what qualified bean instance gets injected.
 * This class demonstrates that approach - in the {@link PostConstruct}-method you have to explicitly
 * fetch an instance and when doin' that you'll have to decide what flavour you'd like. That flavour can
 * be changed based i.e. on configuration => you can change injected candidates by configuration and not
 * by changing the concrete annotations.
 *
 * @see UserOfQualifiedBeans
 * @see AnnotationLiteral
 * @see Instance
 */
@Startup
@Singleton
public class ProgrammaticQualifierDecision {

    @Inject @Any
    Instance<IProcessor> processor;

    @PostConstruct
    void init() {
        IProcessor iProcessor = processor.select(new ProcessorQualifier() {}).get();
        System.out.println("Programmatic lookup: " + iProcessor.getClass());
    }

    /**
     * This is just a helper class since our {@link javax.inject.Qualifier}-annotation has custom members. Thus we
     * cannot just inline define an instance of {@link AnnotationLiteral} - we have to explicitly define a subclass.
     * Without custom props it would be possible to write the upper example like this:
     * <pre>
     * {@code IProcessor iProcessor = processor.select(new AnnotationLiteral<Processor> {}).get();}
     * </pre>
     * But the funny thing in this example especially is the custom prop, because now you can now dynamically
     * inject stuff - based on the value which gets returned by {@link ProcessorQualifier#precedence()}
     */
    private class ProcessorQualifier extends AnnotationLiteral<Processor> implements Processor {
        @Override
        public int precedence() {
            /*
             * here you can read any kind of configuration you'd like in order to get the proper precedence-value
             * you need in order to select the proper candidate!
             */
            return 500;
        }
    }
}
