package de.bender;

import de.bender.annotation.IProcessor;
import de.bender.annotation.Processor;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

/**
 * This class injects two times the same type - the only difference between these two types is the
 * {@link Processor}-Qualifier. It's possible to distinguish injection-candidates by using their respective
 * {@link javax.inject.Qualifier} (the candidate has to be annotated with the matching counterpart).<br/>
 * You can use this i.e. to augment the default-implementation and thus decide if you'd like to have the
 * caching-version or the parallel-version of a specific interface-implementation just by annotating the
 * injection-point.
 */
@Startup
@Singleton
public class UserOfQualifiedBeans {

    @Inject
    @Processor
    IProcessor basicProcessor;

    @Inject
    @Processor(precedence = 500)
    IProcessor importantProcessor;

    @PostConstruct
    void init() {
        System.out.println("Qualified instance without value: " + basicProcessor.getClass().getSimpleName());
        System.out.println("Qualified instance with value of 500: " + importantProcessor.getClass().getSimpleName());
    }
}
