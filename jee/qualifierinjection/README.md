# Qualifier Injection

Demonstrates the usage of `Qualifier` when dealing with CDI injection. At least in my projects it's rarely necessary to introduce a custom `Qualifier` and thus, when really needed, it's hardly understood.

This repo contains a dense example which demonstrates the power of custom `Qualifier`-annotations and their usage.
