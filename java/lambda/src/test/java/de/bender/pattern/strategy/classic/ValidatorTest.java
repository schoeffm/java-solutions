package de.bender.pattern.strategy.classic;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;


public class ValidatorTest {

    @Test
    public void validate_withConcreteStrategy_willApplyConfiguredValidation() {
        // given
        Validator allLowerCaseValidator = new Validator(new IsAllLowserCase());
        Validator numericValidator = new Validator(new IsNumeric());

        // when - then
        assertThat(allLowerCaseValidator.validate("abcd"), is(true));
        assertThat(numericValidator.validate("123"), is(true));

    }
}