package de.bender.pattern.strategy.lambda;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;


public class ValidatorTest {

    @Test
    public void validate_withConcreteStrategy_willApplyConfiguredValidation() {
        // given
        Validator allLowerCaseValidator = new Validator(ValidationStrategy.IS_ALL_LOWER_CASE);
        Validator numericValidator = new Validator(ValidationStrategy.IS_NUMERIC);
        Validator allUpperCaseValidator = new Validator(s -> s.matches("[A-Z]+"));

        // when - then
        assertThat(allLowerCaseValidator.validate("abcd"), is(true));
        assertThat(numericValidator.validate("123"), is(true));
        assertThat(allUpperCaseValidator.validate("abcd"), is(false));

    }
}