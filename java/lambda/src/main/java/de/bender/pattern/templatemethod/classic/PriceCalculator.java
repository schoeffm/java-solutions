// Copyright (c) 2011-2018 BMW Group. All rights reserved.

package de.bender.pattern.templatemethod.classic;

import java.math.BigDecimal;

abstract class PriceCalculator {

    public BigDecimal calculatePrice(LineItem i) {

        // externalizes the part that varies
        BigDecimal tax = calculateTaxFor(i);

        // centralizes the part that is common to all variations
        return i.price.multiply(tax);
    }

    abstract BigDecimal calculateTaxFor(LineItem i);


    public static class LineItem {
        BigDecimal price;
    }
}