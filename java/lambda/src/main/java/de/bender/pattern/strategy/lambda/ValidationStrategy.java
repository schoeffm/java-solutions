// Copyright (c) 2011-2018 BMW Group. All rights reserved.

package de.bender.pattern.strategy.lambda;

/**
 * Strategy-Interface which defines the common contract all concrete implementations have to follow.
 */
@FunctionalInterface
public interface ValidationStrategy {

    /**
     * for the usual suspects you can also provide one-line implementations as static constants
     */
    ValidationStrategy IS_NUMERIC =  s -> s.matches("\\d+");
    ValidationStrategy IS_ALL_LOWER_CASE = s -> s.matches("[a-z]+");

    boolean execute(String s);
}
