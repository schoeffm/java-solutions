// Copyright (c) 2011-2018 BMW Group. All rights reserved.

package de.bender.pattern.strategy.classic;

/**
 * Strategy-Interface which defines the common contract all concrete implementations have to follow.
 */
public interface ValidationStrategy {
    boolean execute(String s);
}
