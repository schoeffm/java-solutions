// Copyright (c) 2011-2018 BMW Group. All rights reserved.

package de.bender.pattern.templatemethod.classic;

import java.math.BigDecimal;

public class AmericaTaxedPriceCalculator extends PriceCalculator {

    @Override
    BigDecimal calculateTaxFor(LineItem i) {
        /*
         * some very special calculations for determining the tax
         */
        return new BigDecimal(42L);
    }
}