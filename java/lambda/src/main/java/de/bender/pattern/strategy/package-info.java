/**
 * The implementation of this pattern isn't much different - the only thing you'll have to look closely in
 * this case is that the {@link de.bender.pattern.strategy.lambda.ValidationStrategy}-interface (in other words
 * the interface which defines the strategy-pattern) is a {@link java.lang.FunctionalInterface} since it
 * only defines a single abstract method.
 * <p/>
 * So you should have a look at the client-code - the usage of this pattern which is given in the
 * corresponding unit-tests:
 * <pre>
 * {@code
 * // inline definition using lambda (no dedicated class necessary)
 * Validator v = new Validator(s -> s.matches("\\d+"));
 *
 * // explicit instantiation of a dedicated IsNumeric-class needed
 * // => every validation demands a dedicated class to be implemented
 * Validator v = new Validator(new IsNumeric());
 * }
 * </pre>
 */
package de.bender.pattern.strategy;
