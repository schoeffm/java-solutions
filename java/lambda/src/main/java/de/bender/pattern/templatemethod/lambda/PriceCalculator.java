// Copyright (c) 2011-2018 BMW Group. All rights reserved.

package de.bender.pattern.templatemethod.lambda;

import java.math.BigDecimal;
import java.util.function.Function;

public final class PriceCalculator {

    public BigDecimal calculatePrice(LineItem i, Function<LineItem, BigDecimal> taxProvider) {

        // externalizes the part that varies
        BigDecimal tax = taxProvider.apply(i);

        // centralizes the part that is common to all variations
        return i.price.multiply(tax);
    }

    public static class LineItem {
        BigDecimal price;
    }

}