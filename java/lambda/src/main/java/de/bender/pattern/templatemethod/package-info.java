/**
 * Although the concrete example may be a bit artificial it demonstrates the application of the
 * template-method pattern.<p/>
 * The problem to be solved is that there might be slight variations in an algorithm - so what you'd like
 * to accomplish is to centralize the common part of the algorithm and allow for variations in specific parts
 * of it.<br/>
 * Classically, you define an abstract superclass which defines the common part in terms of a method-implementation
 * (see {@see de.bender.pattern.templatemethod.classic.PriceCalculator}). That method makes then calls to one or more
 * abstract methods which has to be defined by concrete subclasses and represent the variations. For every variant
 * you'd define a concrete subclass which implements just the abstract methods differently.
 * <p/>
 * Using lambdas you can model the common part as a concrete class (not an abstract one - see
 * {@link de.bender.pattern.templatemethod.lambda.PriceCalculator}) and just inject the varying part as lambda (or
 * method reference if it is more complex).
 * <pre>
 * {@code
 * new PriceCalculator().calculatePrice(lineItem, TaxProviderFactory.create(Country.EUROPEAN))
 * // vs.
 * new EuropeanTaxedPriceCalculator().calculatePrice(lineItem)
 * }
 * </pre>
 * The nice thing is that both parts can be kept and maintained separately. In the classic example you'd always have to
 * commingle both concepts/parts and form an inheritance-tree just for the sake of providing an algorithm.
 */
package de.bender.pattern.templatemethod;