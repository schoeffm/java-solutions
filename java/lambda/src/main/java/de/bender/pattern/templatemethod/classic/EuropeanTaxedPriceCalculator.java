// Copyright (c) 2011-2018 BMW Group. All rights reserved.

package de.bender.pattern.templatemethod.classic;

import java.math.BigDecimal;

public class EuropeanTaxedPriceCalculator extends PriceCalculator {

    @Override
    BigDecimal calculateTaxFor(LineItem i) {
        /*
         * complex stuff to be done here
         */
        return new BigDecimal(32L);
    }
}