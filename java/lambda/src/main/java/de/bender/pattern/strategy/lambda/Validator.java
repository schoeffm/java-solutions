// Copyright (c) 2011-2018 BMW Group. All rights reserved.

package de.bender.pattern.strategy.lambda;

/**
 * Client-code for the strategy - the code doesn't <i>know</i> what exact implementation of the
 * {@link ValidationStrategy} is used since it gets the concrete instance injected.
 *
 * Thus, the strategy to be used is decided by the configuration of this class - in this example
 * this is done in the unit-test
 *
 * @see ValidationStrategy
 */
public class Validator {

    private final ValidationStrategy strategy;

    public Validator(ValidationStrategy v) {
        this.strategy = v;
    }

    public boolean validate(String s) {
        return strategy.execute(s);
    }
}