// Copyright (c) 2011-2018 BMW Group. All rights reserved.

package de.bender.pattern.strategy.classic;

/**
 * Concrete implementation of the {@link ValidationStrategy}
 */
public class IsAllLowserCase implements ValidationStrategy {
    @Override
    public boolean execute(String s) {
        return s.matches("[a-z]+");
    }
}