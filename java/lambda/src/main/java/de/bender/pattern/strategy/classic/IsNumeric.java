// Copyright (c) 2011-2018 BMW Group. All rights reserved.

package de.bender.pattern.strategy.classic;

/**
 * Another concrete implementation of the {@link ValidationStrategy}
 */
public class IsNumeric implements ValidationStrategy {
    @Override
    public boolean execute(String s) {
        return s.matches("\\d+");
    }
}